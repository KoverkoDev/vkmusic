package com.kavka.vkmusic.models;

import android.graphics.drawable.Drawable;

/**
 * Created by Error on 11.09.2017.
 */
public class DrawerItem {

    String title;
    Drawable icon;

    public DrawerItem(String title, Drawable icon) {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public Drawable getIcon() {
        return icon;
    }

}
