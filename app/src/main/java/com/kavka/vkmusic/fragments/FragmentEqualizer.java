package com.kavka.vkmusic.fragments;

/**
 * Created by Error on 11.09.2017.
 */

import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.kavka.vkmusic.R;
import com.kavka.vkmusic.activities.DMPlayerBaseActivity;
import com.kavka.vkmusic.manager.MediaController;
import com.kavka.vkmusic.manager.NotificationTask;
import com.kavka.vkmusic.models.SongDetail;
import com.kavka.vkmusic.phonemidea.DMPlayerUtility;
import com.kavka.vkmusic.phonemidea.PhoneMediaControl;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;
import com.thin.downloadmanager.ThinDownloadManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;

public class FragmentEqualizer extends Fragment {

    WebView webView;
    ProgressBar progressBar;
    private ListView recycler_songslist;
    private AllSongsListAdapter mAllSongsListAdapter;
    private ArrayList<SongDetail> songList = new ArrayList<SongDetail>();
    View rootview;
    private DownloadManager mgr=null;
    private long lastDownload=-1L;
    private ThinDownloadManager downloadManager;
    private static final int DOWNLOAD_THREAD_POOL_SIZE = 4;
    private NotificationManager mNotifyManager;
    private Builder mBuilder;
    int id = 0;
    private AdView mAdView;
    ImageView img_bg;


    public FragmentEqualizer() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_equalizer, null);
        setupInitialViews(rootview);
        //new GetAudioVK().execute();
        audioVK();

        MobileAds.initialize(rootview.getContext(), "ca-app-pub-1563785045157371~3565646083");
        mAdView = (AdView) rootview.findViewById(R.id.adviewB);
        AdRequest adRequest = new AdRequest.Builder()
                .build();

        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);
        AdRequest adRequesti = new AdRequest.Builder().build();

        return rootview;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setupInitialViews(View inflatreView) {
        webView = (WebView) inflatreView.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        img_bg = (ImageView) inflatreView.findViewById(R.id.img_bg_fragment);
        Picasso.with(img_bg.getContext()).load("https://pp.userapi.com/c841623/v841623261/492cd/B_EycXmJ14U.jpg").into(img_bg);
        progressBar = (ProgressBar) inflatreView.findViewById(R.id.progressBar);
        recycler_songslist = (ListView) inflatreView.findViewById(R.id.list_all_vk);
        mAllSongsListAdapter = new AllSongsListAdapter(getActivity());
        recycler_songslist.setAdapter(mAllSongsListAdapter);
        mgr=
                (DownloadManager)this.getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

        downloadManager = new ThinDownloadManager(DOWNLOAD_THREAD_POOL_SIZE);
    }

    class GetAudioVK extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            audioVK();
            return null;
        }
    }

    class AudioBrawser extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressBar.setVisibility(View.VISIBLE);
            if(url.contains("client") || url.contains("vk.com")){

            }else {
                webView.setVisibility(View.GONE);
            }
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);

            if(url.contains("client")){

            }else if(url.contains("http://vk-music.download/")) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    webView.setVisibility(View.GONE);

                    view.loadUrl(
                            "javascript:(function() { " +
                                    "var btn = document.createElement('BUTTON');" +
//                                "var text = document.getElementsByClassName('container');" +
//                                "text.style.display='none';"+
                                    "var t = document.createTextNode('dsadsadadsadddddddddddddddddddddddddddddddddddddddddaaasadddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddda');" +
                                    "btn.appendChild(t);" +
                                    "document.body.appendChild(btn);" +
                                    "})()");


                    view.evaluateJavascript("(function() { " +
                            "var t = document.body.innerHTML;" +
                            "return t; })();", new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                            String s = value.toString();


                            try {
                                Document doc = Jsoup.parse("<htmL>" + s + "</html>");
                                String text = doc.toString();
                                String[] ul = text.split("sm2-playlist-bd list-group");
                                String[] ul_no_footer = ul[1].split("footer");
                                String[] text_decoration = ul_no_footer[0].split("text-decoration:none");

                                for(int i = 1 ;i < text_decoration.length; i++){

                                    String name = ""; String url = "";

                                    if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {

                                        String sd = text_decoration[i];
                                        String[] all_li = text_decoration[i].split("u003E");
                                        name = all_li[1].split("u003")[0];
                                        name = name.substring(0, name.length() - 1);

                                        try {
                                            name = name.replace("\\u0430","а");
                                            name = name.replace("\\u0431","б");
                                            name = name.replace("\\u0432","в");
                                            name = name.replace("\\u0433","г");
                                            name = name.replace("\\u0434","д");
                                            name = name.replace("\\u0435","е");
                                            name = name.replace("\\u0451","ё");
                                            name = name.replace("\\u0436","ж");
                                            name = name.replace("\\u0437","з");
                                            name = name.replace("\\u0438","и");
                                            name = name.replace("\\u0439","й");
                                            name = name.replace("\\u0457","ї");
                                            name = name.replace("\\u0456","і");
                                            name = name.replace("\\u043A","к");
                                            name = name.replace("\\u043B","л");
                                            name = name.replace("\\u043C","м");
                                            name = name.replace("\\u043D","н");
                                            name = name.replace("\\u043E","о");
                                            name = name.replace("\\u043F","п");
                                            name = name.replace("\\u0440","р");
                                            name = name.replace("\\u0441","с");
                                            name = name.replace("\\u0442","т");
                                            name = name.replace("\\u0443","у");
                                            name = name.replace("\\u0444","ф");
                                            name = name.replace("\\u0445","х");
                                            name = name.replace("\\u0446","ц");
                                            name = name.replace("\\u0447","ч");
                                            name = name.replace("\\u0448","ш");
                                            name = name.replace("\\u0449","щ");
                                            name = name.replace("\\u044A","ъ");
                                            name = name.replace("\\u044B","ы");
                                            name = name.replace("\\u044C","ь");
                                            name = name.replace("\\u044D","э");
                                            name = name.replace("\\u044E","ю");
                                            name = name.replace("\\u044F","я");

                                            name = name.replace("\\u0410","А");
                                            name = name.replace("\\u0411","Б");
                                            name = name.replace("\\u0412","В");
                                            name = name.replace("\\u0413","Г");
                                            name = name.replace("\\u0414","Д");
                                            name = name.replace("\\u0415","Е");
                                            name = name.replace("\\u0401","Ё");
                                            name = name.replace("\\u0416","Ж");
                                            name = name.replace("\\u0417","З");
                                            name = name.replace("\\u0418","И");
                                            name = name.replace("\\u0419","Й");
                                            name = name.replace("\\u0407","Ї");
                                            name = name.replace("\\u0406","І");
                                            name = name.replace("\\u041A","К");
                                            name = name.replace("\\u041B","Л");
                                            name = name.replace("\\u041C","М");
                                            name = name.replace("\\u041D","Н");
                                            name = name.replace("\\u041E","О");
                                            name = name.replace("\\u041F","П");
                                            name = name.replace("\\u0420","Р");
                                            name = name.replace("\\u0421","С");
                                            name = name.replace("\\u0422","Т");
                                            name = name.replace("\\u0423","У");
                                            name = name.replace("\\u0424","Ф");
                                            name = name.replace("\\u0425","Х");
                                            name = name.replace("\\u0426","Ц");
                                            name = name.replace("\\u0427","Ч");
                                            name = name.replace("\\u0428","Ш");
                                            name = name.replace("\\u0429","Щ");
                                            name = name.replace("\\u042A","Ъ");
                                            name = name.replace("\\u042B","Ы");
                                            name = name.replace("\\u042C","Ь");
                                            name = name.replace("\\u042D","Э");
                                            name = name.replace("\\u042E","Ю");
                                            name = name.replace("\\u042F","Я");

                                            name = name.replace("\\u2116","№");
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        url = all_li[0].split("href=")[1];
                                        url = url.substring(2, url.length() - 2);
                                        url = url.replaceAll("amp;", "");

                                    }else {
                                        String[] all_li = text_decoration[i].split("&gt");
                                        name = all_li[1].split("u003")[0];
                                        name = name.substring(1, name.length() - 1);
                                        url = all_li[0].split("href=")[1];
                                        url = url.substring(2, url.length() - 2);
                                        url = url.replaceAll("amp;", "");
                                    }
                                    String titles[] = name.split("-");
                                    //jcAudios.add(JcAudio.createFromURL(name, url));
                                    SongDetail songDetail = new SongDetail((i-1),1,titles[0], titles[1], url, null, "");
                                    songDetail.setIsCashe(true);
                                    songList.add(songDetail);

                                    String dsd = "";
                                }

                                progressBar.setVisibility(View.GONE);
                                mAllSongsListAdapter.notifyDataSetChanged();
                                //player.initPlaylist(jcAudios);
                                //adapterSetup();
                                //adapterAudio.setCode(1);
                                //adapterAudio.notifyDataSetChanged();
                                //recyclerView.setAdapter(new AdapterAudio(MainActivity.this, jcAudios, MainActivity.this));
                                String dsd = "";
                                //webView.destroy();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    });


                    //checkAdverists();

                } else {
                    progressBar.setVisibility(View.GONE);
                }
            }
        }
    }

    protected void audioVK(){
        //jcAudios.clear();
        webView.setWebViewClient(new AudioBrawser());
        /*webView.setWebViewClient(new WebViewClient(){

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressBar.setVisibility(View.VISIBLE);
                if(url.contains("client") || url.contains("vk.com")){

                }else {
                    webView.setVisibility(View.GONE);
                }
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                progressBar.setVisibility(View.GONE);

                if(url.contains("client")){

                }else if(url.contains("http://vk-music.download/")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        webView.setVisibility(View.GONE);

                        view.loadUrl(
                                "javascript:(function() { " +
                                        "var btn = document.createElement('BUTTON');" +
//                                "var text = document.getElementsByClassName('container');" +
//                                "text.style.display='none';"+
                                        "var t = document.createTextNode('dsadsadadsadddddddddddddddddddddddddddddddddddddddddaaasadddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddda');" +
                                        "btn.appendChild(t);" +
                                        "document.body.appendChild(btn);" +
                                        "})()");


                        view.evaluateJavascript("(function() { " +
                                "var t = document.body.innerHTML;" +
                                "return t; })();", new ValueCallback<String>() {
                            @Override
                            public void onReceiveValue(String value) {
                                String s = value.toString();


                                try {
                                    Document doc = Jsoup.parse("<htmL>" + s + "</html>");
                                    String text = doc.toString();
                                    String[] ul = text.split("sm2-playlist-bd list-group");
                                    String[] ul_no_footer = ul[1].split("footer");
                                    String[] text_decoration = ul_no_footer[0].split("text-decoration:none");

                                    for(int i = 1 ;i < text_decoration.length; i++){

                                        String name = ""; String url = "";

                                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {

                                            String sd = text_decoration[i];
                                            String[] all_li = text_decoration[i].split("u003E");
                                            name = all_li[1].split("u003")[0];
                                            name = name.substring(0, name.length() - 1);

                                            try {
                                                name = name.replace("\\u0430","а");
                                                name = name.replace("\\u0431","б");
                                                name = name.replace("\\u0432","в");
                                                name = name.replace("\\u0433","г");
                                                name = name.replace("\\u0434","д");
                                                name = name.replace("\\u0435","е");
                                                name = name.replace("\\u0451","ё");
                                                name = name.replace("\\u0436","ж");
                                                name = name.replace("\\u0437","з");
                                                name = name.replace("\\u0438","и");
                                                name = name.replace("\\u0439","й");
                                                name = name.replace("\\u0457","ї");
                                                name = name.replace("\\u0456","і");
                                                name = name.replace("\\u043A","к");
                                                name = name.replace("\\u043B","л");
                                                name = name.replace("\\u043C","м");
                                                name = name.replace("\\u043D","н");
                                                name = name.replace("\\u043E","о");
                                                name = name.replace("\\u043F","п");
                                                name = name.replace("\\u0440","р");
                                                name = name.replace("\\u0441","с");
                                                name = name.replace("\\u0442","т");
                                                name = name.replace("\\u0443","у");
                                                name = name.replace("\\u0444","ф");
                                                name = name.replace("\\u0445","х");
                                                name = name.replace("\\u0446","ц");
                                                name = name.replace("\\u0447","ч");
                                                name = name.replace("\\u0448","ш");
                                                name = name.replace("\\u0449","щ");
                                                name = name.replace("\\u044A","ъ");
                                                name = name.replace("\\u044B","ы");
                                                name = name.replace("\\u044C","ь");
                                                name = name.replace("\\u044D","э");
                                                name = name.replace("\\u044E","ю");
                                                name = name.replace("\\u044F","я");

                                                name = name.replace("\\u0410","А");
                                                name = name.replace("\\u0411","Б");
                                                name = name.replace("\\u0412","В");
                                                name = name.replace("\\u0413","Г");
                                                name = name.replace("\\u0414","Д");
                                                name = name.replace("\\u0415","Е");
                                                name = name.replace("\\u0401","Ё");
                                                name = name.replace("\\u0416","Ж");
                                                name = name.replace("\\u0417","З");
                                                name = name.replace("\\u0418","И");
                                                name = name.replace("\\u0419","Й");
                                                name = name.replace("\\u0407","Ї");
                                                name = name.replace("\\u0406","І");
                                                name = name.replace("\\u041A","К");
                                                name = name.replace("\\u041B","Л");
                                                name = name.replace("\\u041C","М");
                                                name = name.replace("\\u041D","Н");
                                                name = name.replace("\\u041E","О");
                                                name = name.replace("\\u041F","П");
                                                name = name.replace("\\u0420","Р");
                                                name = name.replace("\\u0421","С");
                                                name = name.replace("\\u0422","Т");
                                                name = name.replace("\\u0423","У");
                                                name = name.replace("\\u0424","Ф");
                                                name = name.replace("\\u0425","Х");
                                                name = name.replace("\\u0426","Ц");
                                                name = name.replace("\\u0427","Ч");
                                                name = name.replace("\\u0428","Ш");
                                                name = name.replace("\\u0429","Щ");
                                                name = name.replace("\\u042A","Ъ");
                                                name = name.replace("\\u042B","Ы");
                                                name = name.replace("\\u042C","Ь");
                                                name = name.replace("\\u042D","Э");
                                                name = name.replace("\\u042E","Ю");
                                                name = name.replace("\\u042F","Я");

                                                name = name.replace("\\u2116","№");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            url = all_li[0].split("href=")[1];
                                            url = url.substring(2, url.length() - 2);
                                            url = url.replaceAll("amp;", "");

                                        }else {
                                            String[] all_li = text_decoration[i].split("&gt");
                                            name = all_li[1].split("u003")[0];
                                            name = name.substring(1, name.length() - 1);
                                            url = all_li[0].split("href=")[1];
                                            url = url.substring(2, url.length() - 2);
                                            url = url.replaceAll("amp;", "");
                                        }
                                        String titles[] = name.split("-");
                                        //jcAudios.add(JcAudio.createFromURL(name, url));
                                        SongDetail songDetail = new SongDetail((i-1),1,titles[0], titles[1], url, null, "");
                                        songDetail.setIsCashe(true);
                                        songList.add(songDetail);

                                        String dsd = "";
                                    }

                                    progressBar.setVisibility(View.GONE);
                                    mAllSongsListAdapter.notifyDataSetChanged();
                                    //player.initPlaylist(jcAudios);
                                    //adapterSetup();
                                    //adapterAudio.setCode(1);
                                    //adapterAudio.notifyDataSetChanged();
                                    //recyclerView.setAdapter(new AdapterAudio(MainActivity.this, jcAudios, MainActivity.this));
                                    String dsd = "";

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        });


                        //checkAdverists();

                    } else {
                        progressBar.setVisibility(View.GONE);
                    }
                }

            }


        });*/

        webView.loadUrl("https://oauth.vk.com/authorize?client_id=5713007&redirect_uri=http://vk-music.download&response_type=code");

    }

    private void startDownLoad(SongDetail songDetail){
        try {

            new NotificationTask(this.getContext(), songDetail.getTitle()+"_"+songDetail.getTitle(), id, songDetail)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 10);
            id++;

        }catch (Exception e){
            e.printStackTrace();
        }

    }


    public class AllSongsListAdapter extends BaseAdapter {
        private Context context = null;
        private LayoutInflater layoutInflater;
        private DisplayImageOptions options;
        private ImageLoader imageLoader = ImageLoader.getInstance();

        public AllSongsListAdapter(Context mContext) {
            this.context = mContext;
            this.layoutInflater = LayoutInflater.from(mContext);
            this.options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.bg_default_album_art)
                    .showImageForEmptyUri(R.drawable.bg_default_album_art).showImageOnFail(R.drawable.bg_default_album_art).cacheInMemory(true)
                    .cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder mViewHolder;
            if (convertView == null) {
                mViewHolder = new ViewHolder();
                convertView = layoutInflater.inflate(R.layout.inflate_allsongsitem, null);
                mViewHolder.song_row = (LinearLayout) convertView.findViewById(R.id.inflate_allsong_row);
                mViewHolder.textViewSongName = (TextView) convertView.findViewById(R.id.inflate_allsong_textsongname);
                mViewHolder.textViewSongArtisNameAndDuration = (TextView) convertView.findViewById(R.id.inflate_allsong_textsongArtisName_duration);
                mViewHolder.imageSongThm = (ImageView) convertView.findViewById(R.id.inflate_allsong_imgSongThumb);
                mViewHolder.imagemore = (ImageView) convertView.findViewById(R.id.img_moreicon);
                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (ViewHolder) convertView.getTag();
            }
            final SongDetail mDetail = songList.get(position);

            String audioDuration = "";
            try {
                audioDuration = DMPlayerUtility.getAudioDuration(Long.parseLong(mDetail.getDuration()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            mViewHolder.textViewSongArtisNameAndDuration.setText((audioDuration.isEmpty() ? "" : audioDuration + " | ") + mDetail.getArtist());
            mViewHolder.textViewSongName.setText(mDetail.getTitle());
            String contentURI = "content://media/external/audio/media/" + mDetail.getId() + "/albumart";
            imageLoader.displayImage(contentURI, mViewHolder.imageSongThm, options);


            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    SongDetail mDetail = songList.get(position);
                    ((DMPlayerBaseActivity) getActivity()).loadSongsDetails(mDetail);

                    if (mDetail != null) {
                        if (MediaController.getInstance().isPlayingAudio(mDetail) && !MediaController.getInstance().isAudioPaused()) {
                            MediaController.getInstance().pauseAudio(mDetail);
                        } else {
                            MediaController.getInstance().setPlaylist(songList, mDetail, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                        }
                    }

                }
            });

            mViewHolder.imagemore.setColorFilter(Color.DKGRAY);
            if (Build.VERSION.SDK_INT > 15) {
                mViewHolder.imagemore.setImageAlpha(255);
            } else {
                mViewHolder.imagemore.setAlpha(255);
            }

            mViewHolder.imagemore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                       startDownLoad(mDetail);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            return convertView;
        }

        @Override
        public int getCount() {
            return (songList != null) ? songList.size() : 0;
        }

        class ViewHolder {
            TextView textViewSongName;
            ImageView imageSongThm, imagemore;
            TextView textViewSongArtisNameAndDuration;
            LinearLayout song_row;
        }
    }



}
