package com.kavka.vkmusic.fragments;

/**
 * Created by Error on 11.09.2017.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.kavka.vkmusic.R;
import com.kavka.vkmusic.activities.DMPlayerBaseActivity;
import com.kavka.vkmusic.manager.MediaController;
import com.kavka.vkmusic.manager.NotificationTask;
import com.kavka.vkmusic.models.SongDetail;
import com.kavka.vkmusic.phonemidea.DMPlayerUtility;
import com.kavka.vkmusic.phonemidea.PhoneMediaControl;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class FragmentFevorite extends Fragment {

    private static final String TAG = "FragmentFevorite";
    private static Context context;
    private ListView recycler_songslist;
    private AllSongsListAdapter mAllSongsListAdapter;
    private ArrayList<SongDetail> songList = new ArrayList<SongDetail>();
    EditText searchView;
    ProgressBar progressBar;
    Button btn;
    int id = 10;

    public static FragmentFevorite newInstance(int position, Context mContext) {
        FragmentFevorite f = new FragmentFevorite();
        context = mContext;
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragmentchild_mostplay, null);
        setupInitialViews(rootview);
        //loadAllSongs();
        return rootview;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setupInitialViews(View inflatreView) {
        recycler_songslist = (ListView) inflatreView.findViewById(R.id.recycler_allSongs);
        mAllSongsListAdapter = new AllSongsListAdapter(getActivity());
        recycler_songslist.setAdapter(mAllSongsListAdapter);
        searchView = (EditText) inflatreView.findViewById(R.id.editText);
        progressBar = (ProgressBar) inflatreView.findViewById(R.id.progressBar2);
        searchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                searchView.setFocusable(true);
                searchView.setFocusableInTouchMode(true);
                return false;
            }
        });

        btn = (Button) inflatreView.findViewById(R.id.button2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String query = searchView.getText().toString();
                try {
                    query = URLEncoder.encode(query, "utf-8");
                    Log.e("key_down", query);
                } catch (UnsupportedEncodingException e) {

                    e.printStackTrace();
                }
                //editSearch.setText("");
                //checkAdverists();
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                //Toast.makeText(MainActivity.this, editSearch.getText(), Toast.LENGTH_SHORT).show();
                new MyAudioSearch("https://muzofond.com/search/" + query, searchView.getContext()).execute();
            }
        });
    }

    private void loadAllSongs() {
        PhoneMediaControl mPhoneMediaControl = PhoneMediaControl.getInstance();
        PhoneMediaControl.setPhonemediacontrolinterface(new PhoneMediaControl.PhoneMediaControlINterface() {

            @Override
            public void loadSongsComplete(ArrayList<SongDetail> songsList_) {
                songList = songsList_;
                mAllSongsListAdapter.notifyDataSetChanged();
            }
        });
        mPhoneMediaControl.loadMusicList(getActivity(), -1, PhoneMediaControl.SonLoadFor.Favorite, "");
    }

    public class AllSongsListAdapter extends BaseAdapter {
        private Context context = null;
        private LayoutInflater layoutInflater;
        private DisplayImageOptions options;
        private ImageLoader imageLoader = ImageLoader.getInstance();

        public AllSongsListAdapter(Context mContext) {
            this.context = mContext;
            this.layoutInflater = LayoutInflater.from(mContext);
            this.options = new DisplayImageOptions.Builder().showImageOnLoading(R.drawable.bg_default_album_art)
                    .showImageForEmptyUri(R.drawable.bg_default_album_art).showImageOnFail(R.drawable.bg_default_album_art).cacheInMemory(true)
                    .cacheOnDisk(true).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder mViewHolder;
            if (convertView == null) {
                mViewHolder = new ViewHolder();
                convertView = layoutInflater.inflate(R.layout.inflate_allsongsitem, null);
                mViewHolder.song_row = (LinearLayout) convertView.findViewById(R.id.inflate_allsong_row);
                mViewHolder.textViewSongName = (TextView) convertView.findViewById(R.id.inflate_allsong_textsongname);
                mViewHolder.textViewSongArtisNameAndDuration = (TextView) convertView.findViewById(R.id.inflate_allsong_textsongArtisName_duration);
                mViewHolder.imageSongThm = (ImageView) convertView.findViewById(R.id.inflate_allsong_imgSongThumb);
                mViewHolder.imagemore = (ImageView) convertView.findViewById(R.id.img_moreicon);
                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (ViewHolder) convertView.getTag();
            }
            final SongDetail mDetail = songList.get(position);

            String audioDuration = "";
            try {
                audioDuration = DMPlayerUtility.getAudioDuration(Long.parseLong(mDetail.getDuration()));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }

            mViewHolder.textViewSongArtisNameAndDuration.setText((audioDuration.isEmpty() ? "" : audioDuration + " | ") + mDetail.getArtist());
            mViewHolder.textViewSongName.setText(mDetail.getTitle());
            String contentURI = "content://media/external/audio/media/" + mDetail.getId() + "/albumart";
            imageLoader.displayImage(contentURI, mViewHolder.imageSongThm, options);


            convertView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    SongDetail mDetail = songList.get(position);
                    ((DMPlayerBaseActivity) getActivity()).loadSongsDetails(mDetail);

                    if (mDetail != null) {
                        if (MediaController.getInstance().isPlayingAudio(mDetail) && !MediaController.getInstance().isAudioPaused()) {
                            MediaController.getInstance().pauseAudio(mDetail);
                        } else {
                            MediaController.getInstance().setPlaylist(songList, mDetail, PhoneMediaControl.SonLoadFor.All.ordinal(), -1);
                        }
                    }

                }
            });

            mViewHolder.imagemore.setColorFilter(Color.DKGRAY);
            if (Build.VERSION.SDK_INT > 15) {
                mViewHolder.imagemore.setImageAlpha(255);
            } else {
                mViewHolder.imagemore.setAlpha(255);
            }

            mViewHolder.imagemore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    try {
                        startDownLoad(mDetail);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });


            return convertView;
        }

        @Override
        public int getCount() {
            return (songList != null) ? songList.size() : 0;
        }

        class ViewHolder {
            TextView textViewSongName;
            ImageView imageSongThm, imagemore;
            TextView textViewSongArtisNameAndDuration;
            LinearLayout song_row;
        }
    }


    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) &&
                (i == KeyEvent.KEYCODE_ENTER)) {
            // Perform action on key press
            String query = searchView.getText().toString();
            try {
                query = URLEncoder.encode(query, "utf-8");
                Log.e("key_down", query);
            } catch (UnsupportedEncodingException e) {

                e.printStackTrace();
            }
            //editSearch.setText("");
            //checkAdverists();
            InputMethodManager inputMethodManager = (InputMethodManager) this.getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(this.getActivity().getCurrentFocus().getWindowToken(), 0);
            //Toast.makeText(MainActivity.this, editSearch.getText(), Toast.LENGTH_SHORT).show();
            new MyAudioSearch("https://muzofond.com/search/" + query, searchView.getContext()).execute();
            return true;
        }
        return false;
    }

    public class MyAudioSearch extends AsyncTask<String, Void, String> {


        Elements news;
        String url;
        String results = "";
        String fullurlImg = "";
        Context context;
        String fullImg;


        public MyAudioSearch(String url, Context activity) {
            this.url = url;
            this.context = activity;
        }

        @Override
        protected void onPreExecute() {
            //super.onPreExecute();
            //webView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... arg) {

            //jcAudios.clear();

            try {
                Document doc = Jsoup.connect(url).get();
                String s = doc.toString();
                Elements link = doc.select("li.item");

                for (int i = 0; i < link.size(); i++) {

                    Element linkHref = link.get(i).select("li.play").first();
                    String urlLi = linkHref.attr("data-url");
                    Element artistEl = link.get(i).select("span.artist").first();
                    String artist = artistEl.text();
                    Element trackEl = link.get(i).select("span.track").first();
                    String track = trackEl.text();
                    Element durationEl = link.get(i).select("div.duration").first();
                    String duration = durationEl.text();
                    String[] durArr = duration.split(":");
                    int minuts = Integer.parseInt(durArr[0]);
                    int seconds = Integer.parseInt(durArr[1]);
                    int dur = (minuts * 60) + seconds;
                    String sd = "";

                    SongDetail songDetail = new SongDetail((i), 1, artist, track, url, null, "");
                    songDetail.setIsCashe(true);
                    songList.add(songDetail);
                }
                mAllSongsListAdapter.notifyDataSetChanged();

            } catch (Exception e) {
                //Toast.makeText(Horoscope.this,"Проверьте подлючение к интернету",Toast.LENGTH_SHORT).show();
                e.printStackTrace();
                results = "Проверьте подключение к интернету...";
            }


            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            //Toast.makeText(c,arrayList.size(),Toast.LENGTH_SHORT).show();

            if (!results.isEmpty() || songList.size() == 0) {
                //recyclerView.setVisibility(View.GONE);

                Toast.makeText(context, "Ничего не найдено", Toast.LENGTH_SHORT).show();
            } else {
                mAllSongsListAdapter.notifyDataSetChanged();
            }
            progressBar.setVisibility(View.GONE);


        }
    }

    private void startDownLoad(SongDetail songDetail) {
        try {

            new NotificationTask(this.getContext(), songDetail.getTitle() + "_" + songDetail.getTitle(), id, songDetail)
                    .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 10);
            id++;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
