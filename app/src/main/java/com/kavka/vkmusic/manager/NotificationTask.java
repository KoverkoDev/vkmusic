package com.kavka.vkmusic.manager;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.kavka.vkmusic.R;
import com.kavka.vkmusic.activities.AlbumAndArtisDetailsActivity;
import com.kavka.vkmusic.activities.DMPlayerBaseActivity;
import com.kavka.vkmusic.models.SongDetail;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Error on 17.09.2017.
 */
public class NotificationTask extends AsyncTask<Integer, Double, Void> {

    private final static String TAG = NotificationTask.class.getName();
    private NotificationCompat.Builder mBuilder;
    private final Context mContext;
    private final int mId;
    private NotificationManager mNotifyManager;
    private final String mTitle;
    private SongDetail songDetail;

    public NotificationTask(Context context, String title, int id, SongDetail songDetail) {
        mContext = context;
        mTitle = title;
        mId = id;
        this.songDetail = songDetail;
    }

    @Override
    protected Void doInBackground(Integer... params) {
        Log.d(TAG, "doInBackground");

        // waiting 3 seconds so you can see the first notification
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            Log.e(TAG, e.getMessage());
        }

        // the long job
        try {

            final File newFile = new File(mContext.getCacheDir()+"/vkmusic");
            newFile.mkdir();

            URL url = new URL(songDetail.getPath());
            URLConnection connection = url.openConnection();
            connection.connect();
            // this will be useful so that you can show a typical 0-100% progress bar
            int fileLength = connection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream());
            OutputStream output = new FileOutputStream(new File(mContext.getCacheDir()+"/vkmusic",
                    songDetail.getArtist()+"_"+songDetail.getTitle()+
                    ".mp3"));

            byte data[] = new byte[1024];
            long total = 0;
            int count;
            publishProgress(0D);
            while ((count = input.read(data)) != -1) {
                total += count;

                publishProgress((double)(total * 100) / fileLength);
                // publishing the progress....
                //   Bundle resultData = new Bundle();
                //   resultData.putInt("progress" ,(int) (total * 100 / fileLength));
                //   receiver.send(UPDATE_PROGRESS, resultData);
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
            publishProgress(100D);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * called only once
     */
    private void initNotification() {
        mNotifyManager = (NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mBuilder = new NotificationCompat.Builder(mContext);
    }

    @Override
    protected void onPostExecute(Void result) {
        Log.d(TAG, "onPostExecute");
        super.onPostExecute(result);
        // createNotification("completed");
        setCompletedNotification();
    }

    @Override
    protected void onPreExecute() {
        Log.d(TAG, "onPreExecute");
        super.onPreExecute();

        initNotification();

        setStartedNotification();

    }

    @Override
    protected void onProgressUpdate(Double... values) {
        Log.d(TAG, "onProgressUpdate with argument = " + values[0]);
        super.onProgressUpdate(values);

        int incr = values[0].intValue();
        if (incr == 0)
            setProgressNotification();
        updateProgressNotification(incr);

    }

    /**
     * the last notification
     */
    private void setCompletedNotification() {
        mBuilder.setSmallIcon(R.drawable.ic_launcher).setContentTitle(mTitle)
                .setContentText("Завершено: "+songDetail.getArtist()+"_"+songDetail.getTitle()+" ");

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(mContext, AlbumAndArtisDetailsActivity.class);

        // The stack builder object will contain an artificial back stack for
        // the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(DMPlayerBaseActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        //mBuilder.setContentIntent(resultPendingIntent);

        mNotifyManager.notify(mId, mBuilder.build());
    }

    /**
     * the progress notification
     * <p>
     * called only once
     */
    private void setProgressNotification() {
        mBuilder.setContentTitle(mTitle).setContentText("Сохранение "+ songDetail.getArtist()+"_"+songDetail.getTitle())
                .setSmallIcon(R.drawable.ic_launcher);
    }

    /**
     * the first notification
     */
    private void setStartedNotification() {
        mBuilder.setSmallIcon(R.drawable.ic_launcher).setContentTitle(mTitle)
                .setContentText("Начало загрузки.");

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(mContext, DMPlayerBaseActivity.class);

        // The stack builder object will contain an artificial back stack for
        // the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(DMPlayerBaseActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        //mBuilder.setContentIntent(resultPendingIntent);

        mNotifyManager.notify(mId, mBuilder.build());
    }

    /**
     * the progress notification
     * <p>
     * called every 0.1 sec to update the progress bar
     *
     * @param incr
     */
    private void updateProgressNotification(int incr) {
        // Sets the progress indicator to a max value, the
        // current completion percentage, and "determinate"
        // state
        mBuilder.setProgress(100, incr, false);
        // Displays the progress bar for the first time.
        mNotifyManager.notify(mId, mBuilder.build());
        // Sleeps the thread, simulating an operation
        // that takes time
    }
}
