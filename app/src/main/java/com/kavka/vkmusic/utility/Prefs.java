package com.kavka.vkmusic.utility;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Error on 17.09.2017.
 */
public class Prefs {

    Context context;
    private static final String APP_PREFERENCES = "config";
    private static final String APP_PREFERENCES_EXIT = "exit";
    private static final String APP_PREFERENCES_FIRST = "first";
    private SharedPreferences mSettings;

    public Prefs(Context context) {
        this.context = context;
        mSettings = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    public int  getFirst(){
        return Integer.parseInt(mSettings.getString(APP_PREFERENCES_FIRST,"0"));
    }

    public void setFirst(){
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(APP_PREFERENCES_FIRST, String.valueOf(1));
        editor.apply();
    }
    public int  getExit(){
        return Integer.parseInt(mSettings.getString(APP_PREFERENCES_EXIT,"0"));
    }

    public void setExit(int i){
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(APP_PREFERENCES_EXIT, String.valueOf(i));
        editor.apply();
    }

}
