package com.kavka.vkmusic.utility;

/**
 * Created by Error on 11.09.2017.
 */
import android.util.Log;


public class LogWriter {

    public static void debug(String TAG, String writeText) {
        Log.d(TAG, writeText);
    }

    public static void info(String TAG, String writeText) {
        Log.i(TAG, writeText);
    }
}
